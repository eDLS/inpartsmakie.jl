module InPartSMakie

#
#    --       ----                  ,--  --   --      --    .
#    / _ _    /   \  __  _ _  _/_  /_    |\   /|  __  | _  __   __
#   /  /  /  /___/  __ | /    /      /   | \ / |  __| |/    |  |__|
#  /  /  /  /      /  / /    /      /    |  V  | |  | | \   |  |
# -- -- -- --     '--- --   ’-  ---’    ---   --- --' -- - --- '--'
#

using InPartS
using Makie, Makie.Observables
using Colors
using GeometryBasics
using Requires
using InPartSObstacles
using LinearAlgebra
using CoordinateTransformations, Rotations

## Colors

# colors stolen from matplotlib tableau colormap
const PARTICLEBLUE = colorant"#1f77b4"
const DOMAINGREEN  = colorant"#2ca02c"
const OBSTACLEGREY = colorant"#7f7f7f"

# common color functions
"""
    colorbyid(p::AbstractParticle)
Returns a color based on the `id` of the particle.

Colors are drawn from HSL space with hue values ranging from 0 to 1,
saturation values from 0.5 to 1 and lightness from 0.25 to 0.75.
"""
colorbyid(p::AbstractParticle) = colorbyid(p.id)
function colorbyid(id::UInt64)
    hue = mod(id * MathConstants.φ, 1.0)
    sat = 0.5 + mod(id * √2, 0.5)
    light = 0.25 + mod(id * 1/√2, 0.5)

    return RGB(HSL(360hue, sat, light))
end

@deprecate id_based_color colorbyid

# Geometry definitions

include("bboxes.jl")
include("geometry.jl")
include("meshes.jl")
include("edgefinder.jl")

# main plot recipe
include("recipe.jl")

# extras
include("boxgrid.jl")
include("camerapresets.jl")

# more extras
include("insets.jl")

# workaround disabling weird shading selection for our plots (see https://github.com/MakieOrg/Makie.jl/issues/3559)
# delays replacement of `automatic` until after *our* recipies
import Makie.default_shading!
default_shading!(::ParticlePlot, ::Scene) = nothing
default_shading!(::ObstaclePlot, ::Scene) = nothing
default_shading!(::BoxGridPlot, ::Scene) = nothing

include("wheel.jl")

function __init__()
    @require GLMakie="e9467ef8-e4e7-5192-8a1a-b1aee30e663a" begin
        include("interactiveutils.jl")
    end
    # @require StereoMakie="bd60f1c0-7e99-4e4a-a57a-589e0cb1a096" begin
    #    # add stereo 3D stuff here
    #end

end

end # module InPartSMakie
