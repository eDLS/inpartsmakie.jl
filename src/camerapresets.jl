"""
    plan_view!(scene; kwargs...)
Sets up an orthographic camera for a plan view of the plots in scene

## Optional parameters
 * `axis = 1` Observation axis
 * `dir = -1` Observation direction
 * `zoom = 1.0` Zoom factor. `1.0` corresponds to a tight fit
 * `lims` Data limits. Defaults are set using [`data_limits`](@ref)
"""
function plan_view!(sc; axis = 1, dir = -1, zoom = 1.0, lims = data_limits(sc), camdistance = 1)
    @assert !iszero(dir) "dir must be non-zero"

    lookat = lims.origin + lims.widths/2
    spacediagonal = norm(lims.widths)
    eyeposition = collect(lookat)
    eyeposition[axis] += sign(dir)*(spacediagonal/2 + camdistance)

    Makie.Camera3D(sc;
        projectiontype = Makie.Orthographic,
        eyeposition,
        lookat,
        # upvector: either z or y
        upvector = (axis == 3) ? Vec3f(0,1,0) : Vec3f(0,0,1),
        # set camera clipping range to a large enough value
        near = min(0.1, camdistance),
        far = max(1000.0, camdistance + spacediagonal),
    )

    _fit_to_lims(sc, lims, zoom)

    return
end

"""
    isometric_view!(scene; kwargs...)
Sets up an orthographic camera for an isometric view of the plots in scene

## Optional parameters
 * `dir = (1,1,1)` Observation corner. Only the signs of the entries are taken into account
 * `zoom = 1.0` Zoom factor. `1.0` corresponds to a tight fit
 * `lims` Data limits. Defaults are set using [`data_limits`](@ref)
"""
function isometric_view!(sc; dir = (1,1,1), zoom = 1.0, lims = data_limits(sc), camdistance = 1)
    @assert !any(iszero, dir) "dir must be non-zero in all components"

    lookat = lims.origin + lims.widths/2
    spacediagonal = norm(lims.widths)
    eyeposition = lookat .+ sign.(dir).*(spacediagonal/2 + camdistance)

    Makie.Camera3D(sc;
        projectiontype = Makie.Orthographic,
        eyeposition,
        lookat,
        # set camera clipping range to a large enough value
        near = camdistance,
        far = @show(camdistance + 2spacediagonal),
    )

    _fit_to_lims(sc, lims, zoom)

    return
end


function _fit_to_lims(sc, lims, zoom)
    projected_limits = sc.camera.projectionview[]*lims
    zoomprefactor = maximum(projected_limits.widths)/2
    zoom!(sc, zoom*zoomprefactor)
end