using LinearAlgebra

"""
    interactivescene!(fig, sim; kwargs...)
    interactivescene!(gridpos, sim; kwargs...)

Sets up a scene for interactive exploration of the simulation `sim`, including sliders to limit the visible particles to
a specified volume slice and a toggle for obstacle display. Returns
  - the [`SimPlot`](@ref) object
  - the grid layout containing scene, sliders and labels
  - a NamedTuple of `Label`s and sliders/toggles
  - a NamedTuple of the kwargs passed to the main `plot` function

Intended mostly for 3D simulations, but also works in 2D.

## Optional parameters
  - `sliderres`, resoultion of the slice selection sliders
  - `axkw`, a named tuple of keyword arguments for the Axis/LScene used to display the simulation
  - `particles`, `obstacles`, `domain`, passed through to the [`SimPlot`](@ref)
"""
interactivescene!(gridpos::Union{Makie.GridPosition, Makie.GridSubposition}, sim::Simulation; kwargs...) =
    interactivescene!(gridpos, Observable(sim); kwargs...)

function interactivescene!(gridpos::Union{Makie.GridPosition, Makie.GridSubposition}, sim::Observable;
    fig = Makie.get_figure(gridpos),
    sliderres = 0.1,
    axkw = (;),
    particles = (;),
    obstacles = (;),
    domain = (;),
    autoresetbbox = Observable(true)
)
    D = ndims(sim[].domain)

    if D == 3
        @warn """ Because of an upstream issue (https://github.com/MakieOrg/Makie.jl/issues/2475) there may be \
            problems for empty particle vectors, resulting in segfaults in the worst case. Tread lightly, and be \
            aware that your session may crash at any point!
        """
    end
    dsize = Observable(domainsize(sim[])) # do NOT lift, this should be changed manually

    gly = gridpos[] = GridLayout()

    ll_obst = gly[2, 1] = Label(fig, "Display obstacles", tellwidth = true, halign = :right)
    tg_obst = gly[2, 2] = Toggle(fig, active = true, tellwidth = false, halign = :left)

    ll_radial = gly[3, 1] = Label(fig, "Radial selection", tellwidth = true, halign = :right)
    sl_radial = gly[3, 2] = IntervalSlider(fig; range = @lift(0.0:sliderres:(norm($(dsize))/2)))

    cartesian_limits = []
    dimensions = [:x, :y, :z][1:D]
    for (i, dim) ∈ enumerate(dimensions)
        ll = gly[3 + i, 1] = Label(fig, "$dim Selection", tellwidth = true, halign = :right)
        sl = gly[3 + i, 2] = IntervalSlider(fig; range = @lift(0.0:sliderres:$dsize[i]))
        push!(cartesian_limits, dim => (ll, sl))
    end

    on(dsize) do ds
        # auto reset if limits change
        if autoresetbbox[]
            set_close_to!(sl_radial, 0.0, norm(ds)/2)
            for (i, kv) ∈ enumerate(cartesian_limits)
                set_close_to!(kv[2][2], 0.0, ds[i])
            end
        end
    end


    sc = gly[1, 1:end] = (D == 2) ? Axis(fig; aspect = DataAspect(), axkw...) : LScene(fig; axkw...)
    filterfunction = @lift p -> begin
        rmin, rmax = $(sl_radial.interval)
        radial = rmin ≤ norm(p.centerpos .- $(dsize)/2) < rmax
        return radial
    end

    lobss = map(x->x[2][2].interval, cartesian_limits)
    bbox = map(lobss...) do ls...
        mins = Float32[]
        maxs = Float32[]
        for i ∈ 1:D
            mi, ma = ls[i]
            push!(mins, mi)
            push!(maxs, ma)
        end
        (Point{D, Float32}(mins), Point{D, Float32}(maxs))
    end

    plotkwargs = (;
        particles = (;
            filterfunction,
            bbox,
            particles...
        ),
        obstacles = (;
            visible = tg_obst.active,
            obstacles...
        ),
        domain,
        dsize
    )


    sp = plot!(sc, sim;
        plotkwargs...
    )

    return (sp, gly, (;obstacles = (ll_obst, tg_obst), radial = (ll_radial, sl_radial), cartesian_limits...), plotkwargs)
end

interactivescene!(fig::Makie.Figure, sim; kwargs...) = interactivescene!(fig[1,1], sim; fig, kwargs...)


"""
    interactivescene(sim; figurekwargs = (;), kwargs...)
Creates a new `Figure` with keyword arguments `figurekwargs` and calls [`interactivescene!`](@ref) with the remaining `kwargs`.
Returns
  - the figure
  - the objects returned by [`interactivescene!`](@ref)
"""
function interactivescene(sim; figurekwargs = (;), kwargs...)
    fig = Figure(; figurekwargs...)
    return (fig, interactivescene!(fig, sim; kwargs...)...)
end


"""
    explore_file(df; warn = false, kwargs...)
Loads a simulation from `df` and displays it using [`interactivescene`](@ref), adding
an additional slider for snapshot selection.
Keyword argument `warn = true` enables type reconstruction related warnings, all other kwargs
are passed to [`interactivescene`](@ref)
"""
function explore_file(df; warn = false, kwargs...)
    snaprange = 0:1:(InPartS.numsnaps(df)-1)
    sim = Observable(InPartS.readsim(df; snap = 0, warn))

    fig, sp, ly, sls = interactivescene(sim; kwargs...)


    ll_snap = ly[end+1, 1] = Label(fig, @lift(string("t = ", round(InPartS.current_time($sim), digits = 5))), tellwidth = false, halign = :right)
    sl_snap = ly[end, 2] = Slider(fig, range = snaprange)

    on(sl_snap.value) do snap
        InPartS.readsnap!(sim[], df, snap; warn)
        notify(sim)
    end

    return (fig, (sp, ly, (;sls..., snap = (ll_snap, sl_snap))))
end