"""
    boxgridplot(pv)
Plots the box grid and obstacle boxes for a simulation with obstacles. Useful for debugging.

## Attributes

$(Makie.ATTRIBUTES)

"""
Makie.@recipe(BoxGridPlot, sim) do scene
    Makie.Attributes(;
        markersize = 0.9,
        markersize_empty = 0.9,
        color = nothing,
        color_empty = RGBAf(0.5, 0.5, 0.5, 0.2),
        shading = Makie.Automatic(),
        transparency = Makie.Automatic(),
        colormap = Makie.default_theme(scene, Makie.MeshScatter).colormap
    )
end



function Makie.plot!(boxgridplot::BoxGridPlot)
    Makie.@extract boxgridplot (
        markersize, markersize_empty, color, color_empty, colormap, shading, transparency
    )

    sim = boxgridplot[1]
    D = length(domainsize(sim[]))

    boxgrid = @lift $sim.particles.boxgrid

    boxcentertransform = @lift x->Point(@.($boxgrid.size*(x.I - 0.5)))

    bc_obstacles = @lift Dict(i => $sim.particles.so_boxes[i]  for i ∈ eachindex(InPartS.obstacles($sim)))
    empty_boxes = @lift map($boxcentertransform, setdiff(CartesianIndices($boxgrid.boxes), union(values($bc_obstacles)...)))

    full_boxes = @lift reduce(vcat, map.($boxcentertransform, values($bc_obstacles)))
    colorkey = @lift reduce(vcat, [fill(k, length(v)) for (k,v) ∈ pairs($bc_obstacles)])
    mscolors = @lift _bgp_color_conversion($color, $colorkey, $colormap)

    marker = @lift Makie.HyperRectangle(-Vec($boxgrid.size)/2, Vec($boxgrid.size))

    meshscatter!(
        boxgridplot,
        empty_boxes;
        marker,
        markersize = markersize_empty,
        color = color_empty,
        shading = _forwardshading(shading, D),
        transparency = @lift(($transparency == Makie.automatic) ? (D == 3) : $transparency),

    )



    meshscatter!(
        boxgridplot,
        full_boxes;
        marker,
        markersize,
        color = mscolors,
        shading = _forwardshading(shading, D),
        transparency = @lift(($transparency == Makie.automatic) ? (D == 3) : $transparency),
        colormap
    )

    return boxgridplot
end


_bgp_color_conversion(c, _, _) = c
_bgp_color_conversion(::Nothing, ckey, _) = ckey
_bgp_color_conversion(c::AbstractVector, ckey, _) = [c[mod1(k, end)] for k ∈ ckey]