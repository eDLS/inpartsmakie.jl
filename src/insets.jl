_corners(rect1) = [
    rect1.origin,
    rect1.origin + rect1.widths.*Vec2f(1, 0),
    rect1.origin + rect1.widths.*Vec2f(1, 1),
    rect1.origin + rect1.widths.*Vec2f(0, 1)
]


function cornerpoly(c1, c2)
    # this assumes that c1, c2 are corners of an axis aligned rectangle
    # if that isn't the case things will break
    connections = Point2f[]
    for i ∈ 1:4
        δ = c1[i] - c2[i]
        par = isodd(i) ? -1 : 1
        if *(δ..., par) > 0
            @show metapar = isodd(i÷2) ? -1 : 1
            if δ[1]*metapar*par > 0
                segment = Point2f[c1[mod1(i-1, 4)], c1[i], c2[i], c2[mod1(i+1, 4)]]
                if length(connections) > 0 && connections[end] == segment[2]
                    append!(connections, segment[3:end])
                else
                    append!(connections, segment)
                end
            else
                segment = Point2f[c2[mod1(i-1, 4)], c2[i], c1[i], c1[mod1(i+1, 4)]]
                if length(connections) > 0 && connections[end] == segment[2]
                    append!(connections, segment[3:end])
                else
                    append!(connections, segment)
                end
            end
        end
    end
    if length(connections) > 0 && connections[1] != connections[end]
        push!(connections, connections[1])
    end

    return connections
end

"""
    linkinset(rectpolt, insetscene; kwargs...)
Creates an inset thingy linking a plot of an axis-aligned rectangle (`rectplot`) to an `insetscene`.
Experimental, may break at any point
"""
function linkinset!(rectplot, insetscene; strokecolor = :black, strokewidth = 1, color = :transparent, insetzorder = 2000, kwargs...)
    rectscene = rectplot.parent

    polycorners = lift(
        # we need to know where the inset is
        insetscene.viewport,
        # and where the corresponding rectangle in the main panel is
        rectplot.converted[1],
        # and since the latter is in data coordinates we need to know
        # how to transform it into figure pixel space
        rectscene.camera.projectionview,
        rectscene.viewport
    ) do bbox, refs, datatoclip, vp

        # we want a data space to figure pixel space transformation for main panel
        # I'm certain this exists somewhere in Makie but I couldn't be bothered to
        # look it up since it's fairly easy to do

        # this transforms clip space ((-1, 1) in both axes) to figure pixel space
        # using the axis viewport rectangle
        cliptopixel = let (w1, w2) = vp.widths, (a1, a2) = vp.origin
            Mat4f([
                w1/2  0f0 0f0 (w1/2 + a1)
                 0f0 w2/2 0f0 (w2/2 + a2)
                 0f0  0f0 1f0  0f0
                 0f0  0f0 0f0  1f0
            ])
        end
        #  combine this with the camera projectionview (which transforms data to clip)
        datatopixel = cliptopixel*datatoclip

        # corner selection needs to be more sophisticated for situations where the inset is not southeast of the box
        # refcorners_data = [refs.origin, refs.origin + Vec2f(0.0, refs.widths[2]), refs.origin + refs.widths]
        # refcorners_clip = [(datatopixel*Vec4f(vec..., 0, 1))[1:2] for vec ∈ refcorners_data]
        # Point2f[reverse(refcorners_clip); [bbox.origin, bbox.origin +  Vec2f(bbox.widths[1], 0.0), bbox.origin + bbox.widths]]
        cornerpoly([(datatopixel*SA[c..., 0, 1])[1:2] for c ∈ _corners(refs)], _corners(bbox))
    end

    # plot the big polygon connecting the things
    plt = poly!(insetscene.parent, polycorners; space = :pixel, strokecolor, strokewidth, color, kwargs...)
    # plt = lines!(insetscene.parent, linepoints; kwargs..., space = :pixel, color = strokecolor, linewidth = strokewidth)
    # translate everything so it stacks nicely
    # (use massive values to make sure it works, I don't know what the usual z stack looks like)
    translate!(plt, 0,0, insetzorder)

    return plt
end

