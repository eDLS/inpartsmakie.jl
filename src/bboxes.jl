@inline _bbox_intersect(bb1::Makie.Automatic, bb2::Tuple) = _bbox_intersect(bb2, bb1)
@inline _bbox_intersect(bb1, ::Makie.Automatic) = bb1
@inline _bbox_intersect(bb1::Tuple, bb2::Tuple) = (max.(bb1[1], bb2[1]), min.(bb1[2], bb2[2]))

@inline _in_bbox(val, bbox::NTuple{2}, axis) = bbox[1][axis] ≤ val ≤ bbox[2][axis]
@inline _in_bbox(val, bbox::NTuple{2, Real}, _) = bbox[1] ≤ val ≤ bbox[2]
@inline _in_bbox(val::Point, bbox) = all(_in_bbox(val[i], bbox, i) for i ∈ eachindex(val))

@inline _bbox_corners_2D(bbox::NTuple{2, PT}) where PT<:Point{2} = PT[bbox[1], (bbox[2][1], bbox[1][2]), bbox[2], (bbox[1][1], bbox[2][2])]
@inline _bbox_corners_2D(bbox::NTuple{2}) = _bbox_corners_2D((Point{2, Float32}(bbox[1]), Point{2, Float32}(bbox[2])))

@inline _bbox_corners_3D(bbox::NTuple{2, PT}) where PT<:Point{3} = PT[(bbox[i][1], bbox[j][2], bbox[k][3]) for i ∈ 1:2, j ∈ 1:2, k ∈ 1:2]
@inline _bbox_corners_3D(bbox::NTuple{2}) = _bbox_corners_3D((Point{3, Float32}(bbox[1]), Point{3, Float32}(bbox[2])))

@inline _bbox_boundarypos(bbox::NTuple{2}, axis, side) = bbox[side][axis]
@inline _bbox_boundarypos(bbox::NTuple{2, Real}, _, side) = bbox[side]
