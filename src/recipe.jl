_strip_observables(attr::Attributes) = (k => to_value(v) for (k,v) in pairs(attr))

_forwardshading(shading, D) = @lift(($shading == Makie.automatic && D < 3) ? NoShading : $shading)

const _makie_to_lines = @static isdefined(Makie, :to_lines) ? Makie.to_lines : Makie.to_line_segments

###########################################################################################
## Particles
###########################################################################################


"""
    particleplot(pv)
Plot a vector of `InPartS.AbstractParticle`s.
Currently uses [`InPartSMakie.to_mesh`](@ref) to get a mesh representation of the particle
(although this may change in a later version).
2D particles are optionally plotted with outlines.

## Known bugs

 - for 3D particles, `pv` MUST NOT be empty. This is due to an upstream issue with the `Makie.mesh`
   plotting function (see https://github.com/MakieOrg/Makie.jl/issues/2475)
 - for 2D particles, the `strokestyle` attribute is currently broken for mysterious reasons

## Attributes

$(Makie.ATTRIBUTES)

"""
Makie.@recipe(ParticlePlot, pv) do scene
    Makie.Attributes(;
        color = coloralpha(PARTICLEBLUE, 1.0), # should be alpha = 0.7 for 2D
        colormap = :cyclic_mygbm_30_95_c78_n256_s25,
        colorrange = (0, 1),
        colorfunction = nothing,
        quality = 10,
        bbox = Makie.Automatic(),
        shading = Makie.Automatic(),
        tooltipfunction = defaulttooltip,
        filterfunction = Returns(true),
        # for periodic copies?
        offset = nothing,
        # stroke params (2D only)
        strokecolor = :black,
        strokewidth = 1.5,
        strokestyle = nothing,
        # additional kwargs for to_mesh
        meshkwargs = (;),
        # Use poly! recipe instead of mesh! for 2D Cairo plots (by default)
        usepoly = (Symbol(Makie.CURRENT_BACKEND[])==:CairoMakie),
        # passthrough to mesh
        filter(
            kv -> kv[1] ∉ [:color, :colormap, :colorrange, :shading],
            Makie.default_theme(scene, Makie.Mesh).attributes
        )...
    )
end

Makie.plottype(::AbstractVector{<:InPartS.AbstractParticle}) = ParticlePlot

function Makie.plot!(particleplot::ParticlePlot)
    ptype = eltype(eltype(particleplot[1]))
    _particle_recipe!(particleplot, ptype)
end

function _particle_recipe!(particleplot::ParticlePlot, ::Type{<:AbstractParticle})
    Makie.@extract particleplot (
        color, colorfunction, quality, bbox, offset, strokewidth, strokecolor, strokestyle,
        shading, filterfunction, usepoly, meshkwargs
    )
    particles = particleplot[1]



    D = ndims(eltype(eltype(particles)))
    MT = D == 2 ? Mesh2D : Mesh3D


    safeoffset = map(offset) do o
        isnothing(o) ? Vec{D, Float32}(0) : o
    end

    visible_particles = @lift _filter_particles($particles, $bbox; offset = $safeoffset, customfilter = $filterfunction)

    meshes = @lift begin
        if D == 3 && length($visible_particles) == 0
            # FIXME: if this issue won't be resolved soon, we'll have to come up with another way of doing it
            # possibly be wrapping colorfunction in a thing that can also return an RGBA value for some inputs
            # (so that the type of the color array still gets inferred correctly) and then plot a NOMESH with
            # some RGBA color
            error(
                """
                InPartSMakie currently doesn't support plotting empty 3D particle vectors due to an upstream issue \
                (https://github.com/MakieOrg/Makie.jl/issues/2475) that will cause an error/potentially even a segfault \
                as soon as particles are added.

                Make sure that there is at least one particle in the vector when calling `plot` (e.g. by calling\
                `InPartS.flushadditions!` on your simulation before plotting).
                """
            )
        end
        _get_meshes(
            MT, $visible_particles;
            $(meshkwargs)...,
            quality = $quality,
            bbox = ($bbox == Makie.automatic) ? (-Inf, Inf) : $bbox,
            offset = $safeoffset,
        )
    end


    color = @lift _particle_color_conversion($visible_particles, $color, $colorfunction)

    if usepoly[] == true
        # Use the Makie poly function instead of plotting meshes directly
        # This necessary for properly plotting semitransparent particles in CairoMakie
        D == 2 || @warn "Polygon plotting only supported in 2D"

        ptype = typeof(Makie.Polygon([Point2f(0.0,0.0), Point2f(1.0,1.0)]))
        ps = @lift ptype[ Makie.Polygon(convert(Vector{Point2f},_makie_to_lines(mesh)))
                    for mesh in $meshes ]

        Makie.poly!(
            particleplot,
            ps;
            color = color,
            shading = _forwardshading(shading, D),
            # passthrough of all unmodified attributes
            filter(
                kv -> kv[1] ∉ Set([:color, :shading, :strokewidth, :strokecolor]),
                _attributes_for_recipe(Makie.Poly, particleplot.attributes.attributes)
            )...
        )
    else
        Makie.mesh!(
            particleplot,
            meshes;
            color,
            shading = _forwardshading(shading, D),
            # passthrough of all unmodified attributes
            filter(
                kv -> kv[1] ∉ Set([:color, :shading]),
                _attributes_for_recipe(Makie.Mesh, particleplot.attributes.attributes)
            )...
        )
    end

    if D == 2
        Makie.lines!(
            particleplot,
            @lift closed_line_segments($meshes);
            color = strokecolor,
            linestyle = strokestyle,
            linewidth = strokewidth,
            transparency = particleplot.transparency
        )
    end

    # may this never be needed again
    # onany(particles, color, colorfunction, quality, shading, bbox) do p,c,cf,q,s,b
    #     newcolors = _pp_color_conversion(p, c, cf)
    #     pp.color.val = newcolors
    #     pp.shading = (s == Makie.automatic) ? (D == 3) : s
    #     pp[1][] = _maybe_get_meshes(MT, p; quality = q, bbox = (b == Makie.automatic) ? (-Inf, Inf) : b)
    #     @assert all(newcolors .== _pp_color_conversion(p, c, cf)) "color shenaningans"
    #     (D == 2) && (pm[1][] = Makie.to_line_segments(pp[1].val))
    # end

    return particleplot
end

# TODO: rename, this is no longer a maybe
_get_meshes(MT, data; bbox = (-Inf, Inf), kwargs...) = MT[to_mesh(x; bbox, kwargs...) for x ∈ data]
_particle_color_conversion(particles, color, colorfunction) = isnothing(colorfunction) ? color : [colorfunction(p) for p ∈ particles]

_filter_particles(particles, bbox; customfilter = Returns(true), kwargs...) = filter(p -> (isinbbox(p, bbox; kwargs...) && customfilter(p)), particles)
# 1st method to get rid of stray Makie types in the bbox
isinbbox(p::AbstractParticle{N}, bbox::Makie.Automatic; kwargs...) where N = isinbbox(p, (Point{N, Float32}(-Inf), Point{N, Float32}(Inf)); kwargs...)
# 2nd method as generic fallback
isinbbox(p::AbstractParticle, bbox::Tuple; offset = nothing, kwargs...) = if isnothing(offset)
    all(@. bbox[1] ≤ p.centerpos ≤ bbox[2])
else
    all(@. bbox[1] ≤ p.centerpos + offset ≤ bbox[2])
end

defaulttooltip(p::InPartS.AbstractParticle) = string(typeof(p), "\nParticle ID ", p.id)

function closed_line_segments(meshes::AbstractVector{<:GeometryBasics.Mesh{2}})
    line = Point2f[]
    #sizehint!(line, length(meshes)*length(meshes[1].position))
    for (i, mesh) in enumerate(meshes)
        points = _makie_to_lines(mesh)
        append!(line, points)
        # ensure lines are closed
        (points[1] == points[end]) || push!(line, points[1])
        # dont need to separate the last line segment
        if i != length(meshes)
            push!(line, Point2f(NaN))
        end
    end
    return line
end


# applies some reasonable modifications to the default theme to adapt it
# for secondary particles (i.e. higher-order periodic images)
# mostly reducing opacity
_secondarymod(theme, alpha = Observable(0.2)) =  Makie.Attributes(;
    alpha = alpha,
    colormap = @lift(cgrad($(theme.colormap); alpha = $alpha)),
    color = map(theme.color, alpha) do col, alpha
        if col isa Colorant
            return coloralpha(col, alpha)
        elseif col isa AbstractArray && eltype(col) <: Colorant
            return coloralpha.(col, alpha)
        elseif col isa Symbol
            return (col, alpha)
        else
            return col
        end
    end,
    filter(
        kv -> kv[1] ∉ [:color, :colormap],
        theme.attributes
    )...
)

function Makie.show_data(inspector::DataInspector, plot::ParticlePlot, idx, ::Makie.Mesh)
    # Get the tooltip plot
    tt = inspector.plot

    # Get the scene the plot lives in
    scene = Makie.parent_scene(plot)

    # get the selected particle
    particleidx = _vertex_to_meshidx(plot.plots[1], idx)
    particle = _find_filtered_particle(plot, particleidx)
    if ismissing(particle)
        return false
    end
    # Get the hovered data-space position
    pos = particle.centerpos#plot.plots[1].plots[1][1][].position[idx]
    # project to screen space and shift it to be correct on the root scene
    proj_pos = Makie.shift_project(scene, plot.plots[1].plots[1], to_ndim(Point3f, pos, 0))
    # anchor the tooltip at the projected position
    Makie.update_tooltip_alignment!(inspector, proj_pos)

    # Update the final text of the tooltip.
    if haskey(plot, :inspector_label)
        @warn "ParticlePlot ignores the `inspector_label` attribute for now, please use `tooltipfunction(::AbstractParticle)` instead" maxlog=1
	end
    tt.text[] = plot[:tooltipfunction][](particle)
	# Show the tooltip
    tt.visible[] = true

    # return true to indicate that we have updated the tooltip
    return true
end

function _vertex_to_meshidx(plot, idx)
    vertindices = [0; cumsum(map(x->length(x.position), plot[1][]))]
    for i ∈ eachindex(vertindices)
        if vertindices[i] < idx ≤ vertindices[i+1]
            return i
        end
    end
    return -1
end

# KLUDGE: we wouldn't need to do an iterative search if we were to store the filtered particle
# vector somewhere in the plot hierarchy, but I fear that would require a second recipe and
# layered plots
function _find_filtered_particle(plot, idx)
    particles = plot[1][]
    D = ndims(eltype(particles))
    offset = isnothing(plot[:offset][]) ? Vec{D, Float32}(0) : plot[:offset][]

    current = 0
    for p ∈ particles
        if isinbbox(p, plot[:bbox][]; offset) && plot[:filterfunction][](p)
            current += 1
            if current == idx
                return p
            end
        end
    end
    return missing
end


###########################################################################################
## Obstacles
###########################################################################################

"""
    obstacleplot(ov)
Plot a vector of `InPartS.AbstractObstacles`s.
Currently uses [`InPartSMakie.to_mesh`](@ref) to get a mesh representation of the obstacle
(although this may change in the release version).

Note that some infinitely sized obstacles may require passing an appropriately sized `bbox`.

## Attributes

$(Makie.ATTRIBUTES)

"""
Makie.@recipe(ObstaclePlot, ov) do scene
    Makie.Attributes(;
        color = coloralpha(OBSTACLEGREY, 1.0),
        quality = 10,
        bbox = Makie.Automatic(),
        shading = Makie.Automatic(),
        # stroke params (2D only)
        strokecolor = :black,
        strokewidth = 0,
        strokestyle = nothing,
        # additional kwargs for to_mesh
        meshkwargs = (;),
        # Use poly! recipe instead of mesh! for 2D Cairo plots (by default)
        usepoly = (Symbol(Makie.CURRENT_BACKEND[])==:CairoMakie),
        # passthrough to mesh
        filter(
            kv -> kv[1] ∉ Set([:color,:shading]),
            Makie.default_theme(scene, Makie.Mesh).attributes
        )...
    )
end

Makie.plottype(::Vector{<:InPartS.AbstractObstacle}) = ObstaclePlot


function Makie.plot!(obstacleplot::ObstaclePlot)
    Makie.@extract obstacleplot (color, quality, shading, bbox, strokewidth, strokecolor,
        strokestyle, usepoly, meshkwargs)

    obstacles = obstacleplot[1]
    D = ndims(eltype(eltype(obstacles)))
    MT = D == 2 ? Mesh2D : Mesh3D
    obstaclemeshes = @lift _get_meshes(MT, $obstacles;
        $(meshkwargs)...,
        quality = $quality,
        bbox = ($bbox == Makie.automatic) ? (-Inf, Inf) : $bbox,
    );

    if usepoly[] == true
        # Use the Makie poly function instead of plotting meshes directly
        # This necessary for properly plotting semitransparent particles in CairoMakie
        D == 2 || @warn "Polygon plotting only supported in 2D"

        ptype = typeof(Makie.Polygon([Point2f(0.0,0.0), Point2f(1.0,1.0)]))
        ps = @lift reduce(vcat, _mesh2polygon.($obstaclemeshes), init=ptype[])

        Makie.poly!(
            obstacleplot,
            ps;
            color = color,
            shading = _forwardshading(shading, D),
            # passthrough of all unmodified attributes
            filter(
                kv -> kv[1] ∉ Set([:color, :shading, :strokewidth, :strokecolor]),
                _attributes_for_recipe(Makie.Poly, obstacleplot.attributes.attributes)
            )...
        )
    else
        Makie.mesh!(
            obstacleplot,
            obstaclemeshes;
            color,
            shading = _forwardshading(shading, D),
            # passthrough of all unmodified attributes
            filter(
                kv -> kv[1] ∉ Set([:color, :shading]),
                _attributes_for_recipe(Makie.Mesh, obstacleplot.attributes.attributes)
            )...
        )
    end
    if D == 2
        Makie.lines!(
            obstacleplot,
            @lift _obstaclecontours($obstaclemeshes);
            color = strokecolor,
            linestyle = strokestyle,
            linewidth = strokewidth,
            transparency = obstacleplot.transparency
        )
    end
    # may this never be needed again
    # onany(obstacles, color, quality, shading, bbox) do p,c,q,s,b
    #     pp.color.val = c
    #     pp.shading = (s == Makie.automatic) ? (D == 3) : s
    #     pp[1][] = _maybe_get_meshes(MT, p; quality = q, bbox = (b == Makie.automatic) ? (-Inf, Inf) : b)

    # end

    return obstacleplot
end

_obstaclecontours(meshes) = isempty(meshes) ? Point2f[] : mapreduce(mv -> [_find_connected_lines(mv); [Point2f(NaN)]], vcat, meshes)


###########################################################################################
## Domains
###########################################################################################

"""
    domainplot(domain)
Plot an `InPartS.AbstractDomain` as a wireframe cube.
In periodic 2D domains, dashed lines are used to represent periodic boundaries.

## Attributes

$(Makie.ATTRIBUTES)

"""
Makie.@recipe(DomainPlot, domain) do scene
    Makie.Attributes(;
        color = DOMAINGREEN,
        # passthrough to LineSegments
        filter(
            kv -> kv[1] ∉ Set([:color, :linestyle]),
            Makie.default_theme(scene, LineSegments).attributes
        )...
    )
end
Makie.plottype(::InPartS.AbstractDomain) = DomainPlot


function Makie.plot!(domainplot::DomainPlot)
    domain = domainplot.domain

    Makie.linesegments!(
        domainplot,
        map(_domain_linesegments, domain);
        linestyle = :solid,
        # passthrough of all unmodified attributes
        filter(
            kv -> kv[1] ∉ Set([:linestyle]),
            _attributes_for_recipe(Makie.LineSegments, domainplot.attributes.attributes)
        )...
    )

    return domainplot
end



#  HACK: 2D domains with seperate line styles can't use the generic implementations,
#  since linesegments doesn't accept multiple line styles (https://github.com/MakieOrg/Makie.jl/issues/1598).
#  If this ever gets fixed, we can add a linestylefunction to the attributes and
#  get rid of this seperate plot function
function Makie.plot!(domainplot::DomainPlot{<:Tuple{InPartS.Domain2D}})
    domain = domainplot[1]

    dls = map(_domain_linesegments, domain)
    dlsx = map(x->x[1:2:end], dls)
    dlsy = map(x->x[2:2:end], dls)
    lss = map(domain) do d
        dsx, dsy  = InPartS.getboundaries(d)
        return (
            dsx <: PeriodicBoundary ? :dash : :solid,
            dsy <: PeriodicBoundary ? :dash : :solid
        )
    end


    Makie.linesegments!(
        domainplot,
        dlsx;
        linestyle = map(last, lss),
        # passthrough of all unmodified attributes
        filter(
            kv -> kv[1] ∉ Set([:linestyle]),
            _attributes_for_recipe(Makie.LineSegments, domainplot.attributes.attributes)
        )...
    )

    Makie.linesegments!(
        domainplot,
        dlsy;
        linestyle = map(first, lss),
        # passthrough of all unmodified attributes
        filter(
            kv -> kv[1] ∉ Set([:linestyle]),
            _attributes_for_recipe(Makie.LineSegments, domainplot.attributes.attributes)
        )...
    )

    return domainplot

end


_domain_linesegments(dm::InPartS.Domain2D) = let (w,h) = InPartS.domainsize(dm)
    GeometryBasics.Point2f[
        (0, 0), (w, 0),
        (w, 0), (w, h),
        (w, h), (0, h),
        (0, h), (0, 0),
    ]
end

_domain_linesegments(dm::InPartS.Domain3D) = let (w,h,d) = InPartS.domainsize(dm)
    GeometryBasics.Point3f[
        (0, 0, 0), (w, 0, 0),
        (w, 0, 0), (w, h, 0),
        (w, h, 0), (0, h, 0),
        (0, h, 0), (0, 0, 0),
        (0, 0, d), (w, 0, d),
        (w, 0, d), (w, h, d),
        (w, h, d), (0, h, d),
        (0, h, d), (0, 0, d),
        (0, 0, 0), (0, 0, d),
        (w, 0, 0), (w, 0, d),
        (w, h, 0), (w, h, d),
        (0, h, 0), (0, h, d)
    ]
end


###########################################################################################
## Simulations
###########################################################################################

"""
    simplot(sim)
Plots an `InPartS.Simulation`, using [in order] [`particleplot`](@ref), [`obstacleplot`](@ref)
and [`domainplot`](@ref).
Has all the properties and issues described in the respective docstrings, plus some additional
ones for fun!

## Secondary particles:

For plotting periodic systems, `simplot` provides the option of adding additional `particleplot`s
to represent the periodic images.
These additional plots are added before the primary `particleplot` and use a modified style, which
can be controlled using the `secondarystyle` attribute, which generally accepts the same arguments
as `particleplot`.
`secondarystyle` defaults to copying the primary particle style, with all patch colors rendered with
reduced opacity.

Secondary particles are added automatically only for 2D periodic domains, but this behaviour can be
overriden by passing an explicit list of offsets in the attribute `secondarycopies`.
Note that secondary particles are only plotted within the global `bbox`, which may need to be explicitely
set to provide optimal results.

## Attributes

$(Makie.ATTRIBUTES)

"""
Makie.@recipe(SimPlot, sim) do scene
    Makie.Attributes(
        particles = Makie.default_theme(scene, ParticlePlot),
        obstacles = Makie.default_theme(scene, ObstaclePlot),
        domain    = Makie.default_theme(scene, DomainPlot),
        bbox = Makie.Automatic(),
        secondarycopies = Makie.Automatic(),
        secondarystyle = Makie.Attributes(alpha = 0.3) # <-- this is annoying because you have to set everything you might want to change later
    )
end
Makie.plottype(::Simulation{PTC}) where {PTC <: InPartS.AbstractParticleContainer} = SimPlot
Makie.boundingbox(sp::SimPlot, space::Symbol = :data) = Makie.boundingbox(sp.plots[end]::DomainPlot, space)
Makie.data_limits(sp::SimPlot) = Makie.data_limits(sp.plots[end]::DomainPlot)


# obstacles, large objects, etc, go in here
function _additionalplots!(plot, sim::Observable{<:Simulation}; bbox = plot[:bbox])
    Makie.plot!(plot, map(obstacles, sim); bbox, filter(x->x[1] != :bbox, plot[:obstacles])...)
    return
end

# no obstacles no problems
function _additionalplots!(::Any, ::Observable{<:Simulation{<:SimpleParticleContainer}}; kwargs...) end

# obstacles + large objects
function _additionalplots!(plot, sim::Observable{<:Simulation{<:POLOContainer}}; bbox = plot[:bbox])
    # plot LargeObjects with same params as obstacles (I'm sure this can't break)
    particleplot!(plot, map(x->x.particles.lov, sim); bbox, filter(x->x[1] != :bbox, plot[:obstacles])...)
    Makie.plot!(plot, map(obstacles, sim); bbox, filter(x->x[1] != :bbox, plot[:obstacles])...)
    return
end


###

function Makie.plot!(plot::SimPlot)
    sim = plot.sim
    bbox = plot.bbox


    bbox_transformed = map(bbox, sim) do bb, sim
        if bb == Makie.Automatic()
            return _sensible_bbox(sim)
        else
            return bb
        end
    end

    particle_bbox = @lift _bbox_intersect($bbox_transformed, $(plot[:particles].bbox))

    # the secondarycopies thing must be static because it affects the number of subplots
    # and I can't be bothered to make it reactive.
    pcs = plot.secondarycopies[]
    periodicoffsets = []
    if pcs == Makie.Automatic()
        periodicity = InPartS.isperiodic(sim[].domain)
        if length(periodicity) == 2
            # only do auto secondarycopies for 2D (because they look awful in 3D)
            dsize = domainsize(sim[])
            for (i, isp) ∈ enumerate(periodicity)
                vec = [0f0, 0f0]
                vec[i] = dsize[i]
                isp && append!(periodicoffsets, [Vec2f(vec), Vec2f(-vec)])
            end
            # "tertiary" copies for full periodicity
            if all(periodicity)
                append!(periodicoffsets, [Vec2f(dsize), Vec2f(dsize[1], -dsize[2]), Vec2f(-dsize), Vec2f(-dsize[1], dsize[2])])
            end
        end
    else
        periodicoffsets = pcs
    end

    if length(periodicoffsets) > 0

        secondarystyle = merge!(filter(kv->kv[1] ≠ :alpha, plot[:secondarystyle]), _secondarymod(plot[:particles], plot[:secondarystyle].alpha))
        for poff ∈ periodicoffsets
            safeoffset = map(plot.particles.offset) do o
                isnothing(o) ? Vec{length(poff), Float32}(0) : o
            end
            poff_add = @lift poff + $(safeoffset)
            Makie.plot!(plot, map(particles, sim);  offset = poff_add, bbox = particle_bbox, filter(x->x[1] ∉ [:offset, :bbox], secondarystyle)...)
        end
    end

    pplot = Makie.plot!(plot, map(particles, sim);  bbox = particle_bbox, filter(x->x[1] != :bbox, plot[:particles])...)
    _additionalplots!(plot, sim; bbox = bbox_transformed)
    Makie.plot!(plot, map(x->x.domain, sim); plot[:domain]...)

    # set attributes for colorbars
    plot[:colormap] = pplot[:colormap]
    plot[:colorrange] = pplot[:colorrange]
    plot[:highclip] = pplot[:highclip]
    plot[:lowclip] = pplot[:lowclip]


    return plot
end

_sensible_bbox(sim::Simulation) = (Pointf{ndims(sim.domain)}(-0.25domainsize(sim)), Pointf{ndims(sim.domain)}(1.25domainsize(sim)))


# this function makes sure that we pass Makie key validation
function _attributes_for_recipe(P::Type{<:Makie.Plot}, kw)
    nameset = Makie.attribute_names(P)
    allowlist = Makie.MakieCore.attribute_name_allowlist()
    deprecations = Makie.MakieCore.deprecated_attributes(P)::Tuple{Vararg{NamedTuple{(:attribute, :message, :error), Tuple{Symbol, String, Bool}}}}
    allowed_keys = union(nameset, allowlist, first.(deprecations))
    # @warn "removed attributes: "*string(filter(kv -> first(kv) ∉ allowed_keys, kw))
    return filter(kv -> first(kv)∈allowed_keys, kw)
end