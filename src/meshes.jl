
# face type
# TODO: this constant exists only for legacy compat and
# can be removed with the next CGModels release
const _FACETYPE = GLTriangleFace

# placeholder 3D mesh
const _NOMESH_3D = GeometryBasics.normal_mesh(fill(Point3f(NaN), 3), [GLTriangleFace(1,2,3)])
const Mesh3D = typeof(_NOMESH_3D)
_emtpy_mesh(::Type{<:Mesh3D}) = _NOMESH_3D

# placeholder 2D mesh
const _NOMESH_2D = GeometryBasics.mesh(fill(Point2f(NaN), 3), facetype = GLTriangleFace)
const Mesh2D = typeof(_NOMESH_2D)
_emtpy_mesh(::Type{<:Mesh2D}) = _NOMESH_2D


"""
    to_mesh(meshtype, obj; quality = 10)

Creates a [`GeometryBasics.Mesh`](@ref) representing the object.

For 2D particles/obstacles, this falls back to using a triangulation of [`InPartS.getpolygon`](@ref).

The keyword argument `quality` is an arbitrary scale for the resolution of the mesh
"""
to_mesh(@nospecialize obj; kwargs...) = error("Objects of type $(typeof(obj)) are not meshable")



###########################################################################################
## various sensible fallbacks for Particles and Obstacles
###########################################################################################

# fallback for 3D obstacles: nothing
to_mesh(obj::InPartS.AbstractObstacle{3}; kwargs...) = _NOMESH_3D

# fallback mesh for 3D particles: a sphere!
to_mesh(obj::InPartS.AbstractParticle{3}; quality = 10, kwargs...) = normal_mesh(
    Tesselation(Sphere(Point3f(obj.centerpos), get(kwargs, :radius, 0.5)), 2quality)
)



# fallback meshes for 2D obstacles and PolyPlottable particles using InPartS.getpolygon
to_mesh(obj::InPartS.AbstractObstacle{2}; kwargs...) = _fallbackmesh2D(obj; kwargs...)

function to_mesh(obj::InPartS.AbstractParticle{2}; quality = 10, kwargs...)
    if InPartS.PlotStyle(typeof(obj)) isa InPartS.IsPolyPlottable
        return _fallbackmesh2D(obj; quality, kwargs...)
    else
        # ultra-fallback: just plot a circle
        return triangle_mesh(Tesselation(Circle(Point2f(obj.centerpos), get(kwargs, :radius, 0.5)), 2quality))
    end
end


function _fallbackmesh2D(obj; quality = 10, offset = Vec2f(0.0), kwargs...)
    outline = InPartS.getpolygon(obj, res = 4*(quality÷2))
    points = [GeometryBasics.Point2f(outline[i, :]) + offset for i ∈ axes(outline)[1]]
    return GeometryBasics.Mesh(
        points,
        GeometryBasics.earcut_triangulate([points])
    )
end

function _fallbackmesh2D(obj::AbstractObstacle{2}; bbox, quality = 10, offset = Vec2f(0.0), kwargs...)
    outline = InPartS.getpolygon(obj, res = 4*(quality÷2))
    points = [GeometryBasics.Point2f(outline[i, :]) + offset for i ∈ axes(outline)[1]]
    if hasfield(typeof(obj), :inverted) && obj.inverted
        return _inversemesh(points, bbox)
    else
        return GeometryBasics.Mesh(
            points,
            GeometryBasics.earcut_triangulate([points])
        )
    end
end

function _inversemesh(loop::Vector{Point2f}, bbox)
    segments = clipsegments(loop, bbox)
    if isempty(segments)
        # the loop is either entirely within or entirely outside the bbox
        if length(loop) > 0 && _in_bbox(loop[1], bbox)
            # at least one point inside => everything inside
            corners = _bbox_corners_2D(bbox)
            return GeometryBasics.Mesh(
                [corners; loop],
                GeometryBasics.earcut_triangulate([corners, loop])
            )
        else
            # everything outside, return placeholder mesh
            return GeometryBasics.Mesh(
                Point2f[],
                GLTriangleFace[]
            )
        end
    end
    #@show segments
    # there are segments — we need to find the corners to complete them
    vertvecs = cliploop!(loop, segments, bbox)
    return (merge ∘ map)(vertvecs) do verts
        GeometryBasics.Mesh(verts, GeometryBasics.earcut_triangulate([verts]))
    end
end


###########################################################################################
## helper functions for inverse loop meshes
###########################################################################################
#=
    clipsegments(loop, bbox)
Iterates through a loop of points and finds out which segments of the loop are part of
the bbox.
Returns a list of pairs containing the indices of the first and last points of the segments.
If the first index is larger than the last, the segment wraps around the end of the point list.
Returns an empty list if the loop never crosses the bbox (including in the case of being entirely
within it!).
=#
function clipsegments(loop::Vector{Point2f}, bbox)
    segments = Pair{Int64, Int64}[]
    was_in_bbox = false
    segment_start = -1
    for i ∈ 1:length(loop)
        p = loop[i]
        is_in_bbox = _in_bbox(p, bbox)
        if !was_in_bbox && is_in_bbox
            segment_start = mod1(i - 1, length(loop))
            was_in_bbox = true
        elseif was_in_bbox && !is_in_bbox
            push!(segments, segment_start => i)
            was_in_bbox = false
        end
    end
    if was_in_bbox
        # we are still in an unclosed segment
        if length(segments) > 0 && segments[1].first == length(loop)
            # if last and first segment can be merged
            segments[1] = segment_start => segments[1].second
        elseif segment_start != length(loop)
            # the segment closes with the first point
            push!(segments, segment_start => 1)
        end
    end
    # if the entire thing never leaves or enters the bbox, there should be no segment returned
    return segments
end


function cliploop!(loop, segments, bbox; clamppoints = true)
    # order of reversed bboxcorners is [top left, top right, bottom right, bottom left]
    bboxcorners = reverse(_bbox_corners_2D(bbox))
    clampdict = Dict{Int64, Point2f}()
    corners = map(segments) do seg
        # find out at which wall the segment starts at
        # the first point is always just outside the bbox
        firstpoint = loop[seg.first]
        fp_clipwalls = _find_clipwalls(firstpoint, bbox)

        lastpoint = loop[seg.second]
        lp_clipwalls = _find_clipwalls(lastpoint, bbox)

        if clamppoints
            clampdict[seg.first]  = clamptoclipwall(firstpoint, loop[mod1(seg.first + 1, end)], fp_clipwalls, bbox)
            clampdict[seg.second] = clamptoclipwall(loop[mod1(seg.second - 1, end)], lastpoint, lp_clipwalls, bbox)
        end

        if all(fp_clipwalls .== lp_clipwalls)
            # case I) fp_clipwalls = lp_clipwalls
            # depending on position along the wall
            if _is_sorted_along_wall(firstpoint, lastpoint, lp_clipwalls)
                println("is sorted")
                # either include no corners
                return Point2f[]
            else
                # or all of them
                return bboxcorners[_cornerpermutation(fp_clipwalls)]
            end
        else
            cp = _cornerpermutation(lp_clipwalls)
            stopwall = _cornerpermutation(fp_clipwalls)[end]
            stopidx = findfirst(==(stopwall), cp)
            return bboxcorners[cp[1:stopidx]]
        end
    end

    # apply clamping
    for (k, v) ∈ pairs(clampdict)
        loop[k] = v
    end

    verts = map(zip(segments, corners)) do (seg, cor)
        # assemble vertices
        [
            !(seg.first < seg.second) ? loop[[seg.first:end; 1:seg.second]] : loop[seg.first:seg.second];
            cor
        ]
    end
    return verts
end

function clamptoclipwall(p, q, clipwalls, bbox)
    #@show clipwalls
    interactionwall = findfirst(clipwalls)
    wallaxis, wallside  = fldmod1(interactionwall, 2)
    return _clamptowall(p, q, _bbox_boundarypos(bbox, wallaxis, wallside), wallaxis)
end

_clamptowall(p::Point, q::Point, wallval::Real, axis::Integer) = let δ = p - q
    ((wallval - q[axis])/δ[axis])* δ + q
end


function _cornerpermutation(clipwalls)
    #@assert sum(clipwalls) == 1 "shit shit shit shit: $clipwalls"
    if clipwalls[1]
        return [1,2,3,4]
    elseif clipwalls[2]
        return [3,4,1,2]
    elseif clipwalls[3]
        return [4,1,2,3]
    elseif clipwalls[4]
        return [2,3,4,1]
    else
        error("invalid clipwalls")
    end
end

_is_sorted_along_wall(point1, point2, clipwalls) = if clipwalls[1]
    point1[2] > point2[2]
elseif clipwalls[2]
    point1[2] < point2[2]
elseif clipwalls[3]
    point1[1] < point2[1]
elseif clipwalls[4]
    point1[1] > point2[1]
else
    error("invalid clipwalls")
end

_find_clipwalls(val, bbox::NTuple{2, <:Point}, axis) = [val ≤ bbox[1][axis], bbox[2][axis] ≤ val]
_find_clipwalls(val, bbox::NTuple{2, Real}, _) = [val ≤ bbox[1], bbox[2] ≤ val]
# for 2D [xlo, xhi, ylo, yhi]
_find_clipwalls(val::Point, bbox) = reduce(vcat, _find_clipwalls(val[i], bbox, i) for i ∈ eachindex(val))



###########################################################################################
## Meshes for InPartSes default obstacles
###########################################################################################


to_mesh(obst::Disk{3}; quality = 15, kwargs...) =
    normal_mesh(Tesselation(GeometryBasics.HyperSphere(Pointf{3}(obst.pos), Float32.(obst.radius)), 3quality))

# TODO: move spherocylindermesh function here and use it for the fN InPartS rod obst

function to_mesh(obst::InfiniteWall{3}; bbox = (Point3f(0), Point3f(1)), backfaces = false, kwargs...)
    poly = intersect_plane_box(obst,_bbox_corners_3D(bbox))
    l = length(poly)
    if l == 0
        return _NOMESH_3D
    end

    # triangulate manually
    tris = [GLTriangleFace(1,2,3)]
    l > 3 && push!(tris, GLTriangleFace(3,4, mod1(5, l)))
    l > 4 && push!(tris, GLTriangleFace(5,1,3))
    l > 5 && push!(tris, GLTriangleFace(5,6,1))

    normals = fill(Vec3f(obst.normal), l)
    # backfaces
    if backfaces
        append!(poly, map(x->x - obst.normal*1000eps(Float32), reverse(poly)))
        append!(normals, -1*normals)
        append!(tris, map(x->GLTriangleFace(x .+ l), tris))
    end
    return GeometryBasics.Mesh(GeometryBasics.meta(poly; normals), tris)
end


function to_mesh(obst::InfiniteWall{2}; bbox = (Point2f(0), Point2f(1)), kwargs...)
    poly = intersect_line_box(obst,_bbox_corners_2D(bbox))
    l = length(poly)
    if l == 0
        return _NOMESH_2D
    end

    # triangulate manually
    tris = [GLTriangleFace(1,2,3)]
    l > 3 && push!(tris, GLTriangleFace(3,4, mod1(5, l)))
    l > 4 && push!(tris, GLTriangleFace(5,1,3))

    return GeometryBasics.Mesh(poly, tris)
end

function to_mesh(obst::FiniteWall3D; kwargs...)
    # triangulate manually
    poly = [Point3f(obst.a...), Point3f(obst.b...), Point3f(obst.c...)]
    tris = [GLTriangleFace(1,2,3)]
    normals = [Vec3f(obst.normal...),Vec3f(obst.normal...),Vec3f(obst.normal...)]

    return GeometryBasics.Mesh(GeometryBasics.meta(poly; normals), tris)
end


InPartSMakie.to_mesh(obst::RodObstacle{3}; quality = 15, offset = Vec3f(0), kwargs...) = spherocylindermesh(
    Float32.(first(obst.backbone)) + offset,
    Float32.(last(obst.backbone)) + offset,
    Float32.(obst.backbone.eφ),
    Float32(obst.radius),
    0f0,
    quality;
    kwargs...
)


"""
    spherocylindermesh
This function needs to be documented because we use it in a different package, so it's technically API.
Therefore, there should be a docstring here
"""
function spherocylindermesh(nn::SVector{3, F}, np::SVector{3, F}, eφ::SVector{3, F}, r::F, α::F, N::Int = 10; nsep = norm(np - nn), kwargs...) where {F <: AbstractFloat}
    # set up various transformations
    # global shift maps (0, 0, 0)ᵀ → negative node position
    globalshift = Translation(Float32.(nn))
    # rotation: maps NORMALS of spherical section around negative node from cooriented CS to global CS
    rot = LinearMap(SArray(RotXZ(Float32(atan(eφ[3], eφ[2])), Float32(acos(eφ[1])),)))
    # rotation and reflection: same thing but with reflection in x (for spherical section NORMALS around positive node)
    rotmir = rot ∘ LinearMap(SA{Float32}[-1 0 0; 0 1 0; 0 0 1])
    # maps POSITIONS of spherical section around negative node from cooriented, comoving CS to global CS
    hcap1t = globalshift ∘ rot ∘ Translation(Float32(nsep), 0f0, 0f0)
    # same as hcap1t but for the other cap
    hcap2t = globalshift ∘ rot ∘ LinearMap(SA{Float32}[-1 0 0; 0 1 0; 0 0 1])

    # set up containers & stuff

    # half_nverts is half the number of vertices in the total mesh
    # N÷2-1 rows of N plus one zenith vertex
    half_nverts = N*(N÷2-1)+1

    # nfaces_cap is the combined number of faces in both caps, excluding the 2N tris in the middle
    # 2N tris between each pair of adjacent rows (of which there are (N÷2)-2 per cap)
    # plus N tris for each of the two the zenith triangle fan
    nfaces_cap = 4N*((N÷2) - 2) + 2N

    verts = Vector{Point3f}(undef, 2half_nverts)
    normals = Vector{Vec3f}(undef, 2half_nverts)
    faces = Vector{GLTriangleFace}(undef, nfaces_cap + 2N)

    # create verts and normals for spherical section around negative node
    i = 1
    @inbounds for θ ∈ range(Float32(α), stop = 0.5f0, length = N÷2)[1:end-1]
        # azimuth loop
        for φ ∈ range(0f0, step = 2f0/N, length = N)
            sθ, cθ = sincospi(θ)
            dir = SA[sθ, cθ.*sincospi(φ)...]
            verts[i] = r*Point3f(dir)
            normals[i] = Vec3f(dir)
            i += 1
        end
    end

    # add zenith point and normal
    verts[i] = Point3f(r, 0, 0)
    normals[i] = Vec3f(1, 0, 0)

    # copy to create verts and normals for spherical section around positive node
    # apply transformations to global CS
    @inbounds for i ∈ 1:half_nverts
        @views verts[i + half_nverts] = hcap2t(verts[i])
        @views normals[i + half_nverts] = rotmir(normals[i])
        @views verts[i] = hcap1t(verts[i])
        @views normals[i] = rot(normals[i])
    end

    # connect verts into tri faces: caps part i (quad grid)
    @inbounds for j ∈ 0:(N÷2)-3
        for i ∈ 1:N
            k = j*2N + 2i - 1
            # this pattern cvorresponds to one quad face, which we have to split up into two triangles
            quadpattern = SA[mod1(i + 1, N) + j*N, i + j*N, i + (j+1)*N, mod1(i + 1, N) + (j+1)*N]
            # for both negative node
            faces[k] = GLTriangleFace(quadpattern[SA[1,2,3]]...)
            faces[k+1] = GLTriangleFace(quadpattern[SA[1,3,4]]...)
            # and the positive node respectively
            faces[nfaces_cap-k] = GLTriangleFace((quadpattern[SA[3,2,1]] .+ half_nverts)...)
            faces[nfaces_cap-k+1] = GLTriangleFace((quadpattern[SA[4,3,1]] .+ half_nverts)...)
        end
    end

    # caps part ii (triangle fan cap around zenith)
    j = (N÷2)-2
    @inbounds for i ∈ 1:N
        k = j*2N + i
        # same as above, but simpler this time since the faces are true triangles and the last
        # vertex (the zenith) does not change
        tripattern = SA[mod1(i + 1, N) + j*N, i + j*N, (j+1)*N+1]
        # negative node
        faces[k] = GLTriangleFace(tripattern...)
        # positive node
        faces[nfaces_cap-k+1] = GLTriangleFace((tripattern .+ half_nverts)...)
    end

    # cylindrical middle bit: this has height 0 for Dumbbells, which creates a discontinuity in the normal vectors,
    # so the internodal crease is visible in shading even if the geometry doesn't resolve it
    @inbounds for i ∈ 1:N
        quadpattern = SA[i, mod1(i + 1, N), mod1(i + 1, N) + half_nverts, i + half_nverts]
        @views faces[end-2N+2i-1] = GLTriangleFace(quadpattern[SA[1,2,3]]...)
        @views faces[end-2N+2i] = GLTriangleFace(quadpattern[SA[1,3,4]]...)
    end

    return GeometryBasics.Mesh(GeometryBasics.meta(verts; normals), faces)
end
