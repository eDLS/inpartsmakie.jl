## Intersections

"""
    intersect_segment_plane(line, plane; kwargs...)
Computes the intersection of a line segment (specified by a vector of two points) and a plane (specified by an object
with fields `support` and `normal`). Returns the intersection point.

Optionally accepts `edgedelta = line[2] - line[1]` and `origindist = plane.support ⋅ plane.normal` as kwargs if they
are already known.
"""
function intersect_segment_plane(line, plane; edgedelta = line[2] - line[1], origindist = plane.support ⋅ plane.normal)
    λ = (origindist - plane.normal ⋅ line[1])/(plane.normal ⋅ edgedelta)
    if 0 ≤ λ ≤ 1
        return line[1] + λ*edgedelta
    else
        return nothing
    end
end

#%%
"""
    intersect_plane_box(plane, boxcorners)
Intersects a plane (specified by an object with fields `support` and `normal` with a box
(specified by a 2x2x2 array of its corners). Returns a vector of 0 or 3-6 points that
form the intersection polygon, right-hand oriented around the plane normal.

Algorithm from Salama and Kolb, "A Vertex Program for Efficient Box-Plane intersection"
"""
function intersect_plane_box(plane, boxcorners::AbstractArray{<:Point3})
    # this is originally a GPU Algorithm and could probably do with some work for optimal CPU running

    # find closest corner along plane normal direction
    boxcornerprojection = map(x->x⋅plane.normal, vec(boxcorners))
    sp = sortperm(boxcornerprojection)
    closestcorner = CartesianIndices(boxcorners)[sp[1]]

    # construct right hand side coordinate system from closestcorner along box edges
    edgevectors = [boxcorners[CartesianIndex(mod1.(Tuple(closestcorner) .+ (1 == i, 2 == i, 3 == i), 2))] - boxcorners[closestcorner] for i ∈ 1:3]
    # if we had to flip an odd number of edge vectors, we have a LHS and have to do an anticyclic permutation
    isodd(sum(Tuple(closestcorner)) - 3) && reverse!(edgevectors)

    # this should NEVER trip
    @assert (edgevectors[1]×edgevectors[2])⋅edgevectors[3] > 0 "not a RHS"

    # create three direct unique paths (that do not share vertices) from the closest to the furthest away points
    # easiest way: cyclic permutations of the edgevectors
    # because they go from the closest to the furthest away point, the plane MUST intersect them
    # if it is somwhere between the closest and furthest points; if it doesn't then it MUST be
    # outside the box
    paths = [
        cumsum([boxcorners[[closestcorner]]; edgevectors[[1,2,3]]]),
        cumsum([boxcorners[[closestcorner]]; edgevectors[[2,3,1]]]),
        cumsum([boxcorners[[closestcorner]]; edgevectors[[3,1,2]]])
    ]

    # the remaining edges are sorted so that the first edge connects the first to the second path,
    # the second edge connects the second to the third and so on
    spareedges = map(paths) do ps
        [ps[3], ps[3] + (ps[1] - ps[2])]
    end

    # compute intersection (max three points)
    polygon = Vector{Union{Nothing, Point3}}(undef, 6)
    for i ∈ 1:3
        # precompute origin distance (HNF of plane)
        origindist = plane.support ⋅ plane.normal
        # short circuiting @something calculates only until the first none-nothing value is found
        polygon[2i-1] = @something(
            intersect_segment_plane(paths[i][1:2], plane; origindist),
            intersect_segment_plane(paths[i][2:3], plane; origindist),
            intersect_segment_plane(paths[i][3:4], plane; origindist),
            Some(nothing)
        )
        # the spare edge intersections must lie in between the associated path intersections
        polygon[2i] = intersect_segment_plane(spareedges[i], plane; origindist)
    end


    polygon = Point3f.(filter(!isnothing, polygon))
end

"""
    intersect_line_box(plane, boxcorners)
Intersects a line (specified by an object with fields `support` and `normal` with a box
(specified by a vector of its corners). Returns a vector of 0 or 3-5 points that
form the intersection polygon, right-hand oriented.

Algorithm adapted from the 3D version [`intersect_plane_box`](@ref).
"""
function intersect_line_box(line, boxcorners::AbstractVector{P}) where {P<:Point2}
    boxcornerprojection = map(x->(x-line.support)⋅line.normal, boxcorners)
    # the final polygon will contain the 2 intersection points and
    # 1-3 box corners (which have to be behind the line)
    nextrapoints = count(<=(0), boxcornerprojection)
    # if there are 0 or 4 points behind the line, the problem is trivial
    if nextrapoints == 0
        return P[]
    elseif nextrapoints == 4
        return boxcorners
    end

    # if not, we sort the points along the normal axis
    sp = sortperm(boxcornerprojection)
    closestcorner = sp[1]

    # construct two paths from the closest to the furthest corner
    # both of which have to be intersected by the line exactly once
    paths = [
        [closestcorner, mod1(closestcorner+1, 4), mod1(closestcorner+2, 4)],
        [closestcorner, mod1(closestcorner-1, 4), mod1(closestcorner-2, 4)]
    ]

    # precompute HNF distance for intersections
    origindist = line.support ⋅ line.normal

    # preallocate polygon (we know how large because of nextrapoints)
    polygon = Vector{P}(undef, 2 + nextrapoints)

    @inbounds begin
        # first path intersection
        polygon[1] = @something(
            intersect_segment_plane(@view(boxcorners[paths[1][1:2]]), line; origindist),
            intersect_segment_plane(@view(boxcorners[paths[1][2:3]]), line; origindist)
        )
        # do we need to add the additional corner from the first path?
        offset = 0
        if boxcornerprojection[paths[1][2]] <= 0
            polygon[2] = boxcorners[paths[1][2]]
            offset += 1
        end
        # add the closest corner (always in poly)
        polygon[2+offset] = boxcorners[closestcorner]
        # as above but for the second path and in reverse
        if boxcornerprojection[paths[2][2]] <= 0
            polygon[3+offset] = boxcorners[paths[2][2]]
            offset += 1
        end
        polygon[3+offset] = @something(
            intersect_segment_plane(@view(boxcorners[paths[2][1:2]]), line; origindist),
            intersect_segment_plane(@view(boxcorners[paths[2][2:3]]), line; origindist)
        )
    end

    return polygon
end
