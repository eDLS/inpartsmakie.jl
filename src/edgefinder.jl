

function _find_connected_lines(mesh, edgeset = _outer_edges(mesh); maxiter = 1000000, cornertrick = true)
    lines = Point2f[]
    if isempty(edgeset)
        return lines
    end
    sizehint!(lines, length(edgeset) + (cornertrick ? 20 : 10)) # heuristic for amount of segments is 10
    pointmap = _build_pointmap(edgeset)
    currentpoint = rand(keys(pointmap))
    lastsegmentstart = 1
    prevpoint = nothing
    movedcursor = false
    while maxiter > 0 && !(isempty(edgeset))
        maxiter -= 1
        #println("current point: $currentpoint")
        # don't go back
        choices = pointmap[currentpoint]

        #println("  found $(length(choices)) neighbours")
        for candidate ∈ choices
            edge = Pair(currentpoint, candidate)

            # println("    candidate $candidate")
            if edge ∈ edgeset
                # println("      found unvisited edge!")
                delete!(edgeset, edge)
                push!(lines, mesh.position[currentpoint+1])
                prevpoint = currentpoint
                currentpoint = candidate
                movedcursor = true
                break
            end
        end

        if !movedcursor
            #println("  looks like we're stuck")
            # we have closed a loop and must pick a new one
            push!(lines, mesh.position[currentpoint+1])
            cornertrick && push!(lines, lines[lastsegmentstart+1])
            push!(lines, Point2f(NaN))

            prevpoint = nothing
            currentpoint = rand(edgeset).first
            lastsegmentstart = length(lines) + 1
        else
            movedcursor = false
        end
    end
    push!(lines, mesh.position[currentpoint+1])
    cornertrick && push!(lines, lines[lastsegmentstart+1])
    return lines
end

function _build_pointmap(edgeset)
    pointmap = Dict{UInt32, Vector{UInt32}}()
    for (p1, p2) ∈ collect(edgeset)
        p1l = get!(pointmap, p1, UInt32[])
        push!(p1l, p2)
    end
    return pointmap
end


function _outer_edges(faces::AbstractVector{GLTriangleFace})
    edgeset = Set{Pair{UInt32, UInt32}}()
    @inbounds for f ∈ faces
        fv = reinterpret(UInt32, f)
        for ep ∈ @views [fv[[1,2]], fv[[2,3]], fv[[3,1]]]
            edge = Pair(minmax(ep...)...)
            if edge in edgeset
                delete!(edgeset, edge)
            else
                push!(edgeset, edge)
            end
        end
    end

    return edgeset
end

function _outer_edges(mesh::Mesh2D)
    facelist = faces(mesh)
    edgeset = Set{Pair{UInt32, UInt32}}()
    @inbounds for f ∈ facelist
        fv = reinterpret(UInt32, f)
        # orientation of the face
        p1 = mesh.position[fv[1]+1]
        p2 = mesh.position[fv[2]+1]
        p3 = mesh.position[fv[3]+1]
        area = (p3-p1) × (p2-p1)
        if area > 0
            edges = SA[Pair(fv[1], fv[2]), Pair(fv[2], fv[3]), Pair(fv[3], fv[1])]
        elseif area < 0
            edges = SA[Pair(fv[1], fv[3]), Pair(fv[3], fv[2]), Pair(fv[2], fv[1])]            
        else
            @warn "Encountered a face with vanishing area. This will cause downstream problems."
            continue
        end
        for edge in edges
            invedge = reverse(edge)
            if invedge in edgeset
                delete!(edgeset, invedge)
            else
                push!(edgeset, edge)
            end
        end
    end

    return edgeset
end



_outer_edges(::Nothing) =  error("no!") #Set{Pair{UInt32, UInt32}}()

# handedness of the loops
# https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
function handedness(loop)
    sum = 0
    N = length(loop)
    for i in 1:N
        sum += (loop[mod1(i+1,N)][1] - loop[i][1]) * (loop[mod1(i+1,N)][2] + loop[i][2])
    end
    return sum > 0
end



# Convert a 2D mesh to an outline Polygon
function _mesh2polygon(mesh::Mesh2D)
    pts = InPartSMakie._find_connected_lines(mesh; cornertrick=false)
    splitidxs = findall(x-> x===Point2f(NaN), pts)
    pushfirst!(splitidxs, 0)
    last(splitidxs) == length(pts) || push!(splitidxs, length(pts)+1)
    loops = [pts[splitidxs[i]+1:splitidxs[i+1]-1] for i in 1:length(splitidxs)-1]
    filter!(!isempty, loops)
    outer = Vector{Point2f}[]
    inner = Vector{Point2f}[]
    for loop in loops
        if handedness(loop)
            push!(outer, loop)
        else
            push!(inner, loop)
        end
    end
    # A polygon must have a single loop as outer boundary
    # However, it can have multiple holes and holes outside the outer boundary
    # appear to be ignored
    return Makie.Polygon.(outer, Ref(inner))
end
