import .Makie:@Block, initialize_block!, fast_deepcopy, make_block_docstring
import .Makie.GeometryBasics
import .Makie.ShaderAbstractions:Buffer, Sampler

@Block Colorwheel begin
    @attributes begin
         "Controls if the color wheel is visible."
        visible = true
        "The colormap of the color wheel."
        colormap = :cyclic_mygbm_30_95_c78_n256_s25
        "Controls how many times the color map is wrapped around the circle"
        cmsymmetry = 1
        """
        Color to fade to towards the inner radius of the wheel. Can be a solid color;
        a real number, which is interpreted as an opacity applied to the color map;
        or `nothing`, which corresponds to an opacity of 1
        """
        innercolor = nothing
        "Resolution of the wheel mesh"
        meshresolution = 8
        "Resolution of the color texture, defaults to meshresolution. In CairoMakie, increasing this setting will not improve your plot"
        colorresolution = nothing
        "The vertical alignment of the color wheel in its suggested boundingbox"
        valign = :center
        "The horizontal alignment of the color wheel in its suggested boundingbox"
        halign = :center
        "The line width of the color wheel's border."
        strokewidth = 1f0
        "Controls if the border of the color wheel is visible."
        strokevisible = true
        "The color of the border."
        strokecolor = RGBf(0, 0, 0)
        "The linestyle of the color wheel border"
        linestyle = nothing
        "The inner radius of the color wheel relative to its size"
        innerradius = 0.3
        "Resolution of the rasterized colorwheel in px per unit. Required so CairoMakie doesn't rasterize the entire figure. Increase if the wheel looks bad"
        rasterize = 1
        "The width setting of the rectangle."
        width = nothing
        "The height setting of the rectangle."
        height = nothing
        "Controls if the parent layout can adjust to this element's width"
        tellwidth = true
        "Controls if the parent layout can adjust to this element's height"
        tellheight = true
        "The align mode of the color wheel in its parent GridLayout."
        alignmode = Inside()
    end
end

function block_docs(::Type{Colorwheel})
    """
    The eDLS Colorwheel, now available as a Makie layout block.

    """
end


function initialize_block!(cw::Colorwheel)
    blockscene = cw.blockscene
    # resolutions for mesh and color
    meshres = lift(lift_res, cw.meshresolution)
    colres = lift(lift_res, cw.colorresolution, meshres)

    # create mesh (& fetch uv buffer)
    cwmesh = lift(cw.innerradius, meshres) do r0, (N, M)
        wheelmesh(r0, 1.0, N,M)
    end
    #cwmesh = @lift $cwmu[1]
    #uvbuf = cwmu[][2]

    # create texture (repeating in angular direction)
    color = lift(cw.colormap, cw.innercolor, colres, cw.cmsymmetry) do cmap, inner, (N,M), sym
        # this repeat hack is stupid but CairoMakie doesn't support my elegant solution with UVMaps
        φcolors = repeat(get(cgrad(cmap), 0:ceil((N-1)÷sym), (0,(N-1)÷sym)), outer = sym)
        if isnothing(inner)
            texture =  reshape(RGBA.(φcolors), 1, :)
        elseif inner isa Real
            opacity = clamp(inner, 0, 1)
            premult = (1 - opacity)*XYZ(RGBf(1,1,1))
            texture = mapreduce(cs -> RGBA.(range(XYZ(cs), opacity*XYZ(cs) + premult, length=M)), hcat, φcolors)
        else
            texture = mapreduce(cs -> RGBA.(range(XYZ(cs), XYZ(Makie.to_color(inner)), length=M)), hcat, φcolors)
        end
        # BUG: updating this observable does not redraw the plot
        # and I think it's the sampler's fault
        # in theory the sampler should probably be outside the observable
        # because it has a constructor that eats observables, but then
        # you get errors on mesh updates

        # sampler doesn't work in CairoMakie
        #Sampler(texture, y_repeat=:repeat)
        texture
    end

    # recompute uv to match desired symmetry
    # DOESN'T WORK IN CAIROMAKIE
    # lift(cw.cmsymmetry, meshres) do sym, (N, M)
    #     update_uv!(uvbuf, N, M, sym)
    #     notify(cwmesh)
    # end

    squarebbox = lift(cw.layoutobservables.computedbbox) do bb
        newwidth = Vec2f(minimum(bb.widths))
        Rect2(ceil.(Int64, bb.origin + (bb.widths - newwidth)/2), ceil.(Int64, newwidth))
    end

    # plot
    content = Scene(blockscene, viewport = squarebbox, clear = false)
    lift(squarebbox, cw.strokewidth) do bbox, sw
        cam = Makie.camera(content)
        padded_size = 1f0 + Float32(sw/minimum(cam.resolution[]))
        Makie.camera(content).projection[] = Makie.orthographicprojection(-padded_size, padded_size, -padded_size, padded_size, -100f0, 100f0)
    end
    # lift(squarebbox) do bb
        # content.viewport = bb
    # end
    Makie.mesh!(content, cwmesh; color, shading = Makie.NoShading, cw.visible, cw.rasterize)
    Makie.lines!(content, Circle(Point2f(0), 1); linewidth = cw.strokewidth, color = cw.strokecolor, cw.linestyle, cw.visible)
end

# resolution lifting, stolen from the cornerradius logic in Makie.Box
function lift_res(res, default = (-1, -1))
    if isnothing(res)
        return default
    elseif res isa NTuple{2, <:Integer}
        return res
    elseif res isa Integer
        return (4res, res) # heuristic for decent angular/radial resolutions
    else
        throw(ArgumentError("Invalid resolution value $mres. Must be an `Integer` or a tuple with 2 `Integer`s."))
    end
end




function wheelmesh(r0, r1, N, M)
    rs = range(r0, r1, length = M)
    relative_rs = range(0, 1, length = M)
    pts   = Point2f.(rs, 0)
    uvmap = Point2f.(0, relative_rs)
    trs = GLTriangleFace[]

    for i in 1:N
        # if i < N
        append!(pts, rs.*((Point2f ∘ reverse ∘ sincospi)(2i/N), ))
        append!(uvmap, Point2f.(i/N, relative_rs))
        # end
        # indices of previous Ip and new In column
        Ip = (1:M) .+ (i-1)*M
        In = (1:M) .+ i*M#(i<N ? i*M : 0)
        for m = 1:M-1
            push!(trs,
                GLTriangleFace(Ip[m],  Ip[m+1], In[m]),
                GLTriangleFace(Ip[m+1],In[m+1], In[m]),
            )
        end
    end

    #uvbuf = Buffer(uvmap)
    GeometryBasics.Mesh(GeometryBasics.meta(pts; uv=uvmap), trs)#, uvbuf
end

# this function made useless by CairoMakie being stupid
# function update_uv!(buf, N, M, φfactor = 1)
#     for i ∈ 0:N, j ∈ 1:M
#         buf[j+i*M] = Point2f(φfactor*i/N, (j-1)/(M-1))
#     end
# end
