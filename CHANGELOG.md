# Changelog

## v0.1.14
 - *bugfix*: the @Block definition for Colorwheel was adapted to internal changes in Makie
 - *bugfix*: automatic limits for simplots once again behave as expected
 - the implementation of particle filtering has been changed to preserve particle vector eltypes

## v0.1.13
 - new attribute `meshkwargs = (;)` in Particle and Obstacle recipes to pass through kwargs to `to_mesh`
 - experimental new feature: `linkinset` can be used to draw a connector polygon between a rectangle area in one subscene and the viewport boundaries of an “inset” subscene
 - *bugfix*: silenced debug messages in colorwheel
 - compat bounds for InPartS v0.7

## v0.1.12
 - *bugfix*: fixed plotting for 3D InfiniteWalls by relaxing dispatch for `intersect_plane_box`
 - *bugfix*: fixed recursive dispatch in 3D bounding box corner calculation

## v0.1.11
 - *new feature*: Colorwheel layoutable
 - compat bounds for InPartS v0.6

## v0.1.10
 - compat bounds for Makie v0.20.1
 - *bugfix*: removed deprecated boolean `shading` kwarg, introduced workaround for https://github.com/MakieOrg/Makie.jl/issues/3559

## v0.1.9
 - stop using `InPartS.hasobstacles` as it is no longer needed
 - `_mesh2polygon` pipeline warns when it encounters bad triangles

## v0.1.8
 - fixed automatic data limits (broken due to upstream change)
 - added nicer plotting for 2D infinite walls using clipping to bbox
 - added automatic plotting for large objects (InPartS POLOContainer)

## v0.1.7
 - compat bounds for new InPartS v0.5 and everything else

## v0.1.6
 - *bugfix* `secondarystyle` kwarg in simplot now actually works
 - added option to use `poly` instead of `mesh` for 2D particles and obstacles (nicer results in CairoMakie)
 - improved error messages for empty 3D simulations

## v0.1.5
 - *bugfix* boxgridplot recipe now actually works

## v0.1.4
 - removed the type piracy from v0.1.3 because Simon Danisch fixed it upstream when Jonas asked him
 - support for FiniteWalls

## v0.1.3
 - "fixed" an unecessary conversion in GeometryBasics for speedup (definitely no piracy involved 😬)
 - added boxgridplot recipe for debugging relevantboxes functions

 ## v0.1.2
- improved obstacle support, now including 3D rod obstacles and InfiniteWalls

## v0.1.1
- Adapt / remove to_mesh definitions for obstacles that are removed from InPartS
## v0.1.0

Initial release, containing
 - a unified plotting recipe for 2D and 3D simulation plots
 - utilities for interactive exploration of 3D simulations
