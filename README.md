# InPartSMakie

Makie recipes for InPartS types.

## Usage

Provides a Makie plot recipe for InPartS particle vectors, obstacle vectors and domains;
as well as a recipe for simulations that combines the three basic recipes into a single plot function call.

Uses Observables whereever reasonable, which simplifies live plotting during evolution to
```julia
fig, ax, simplot = plot(sim;
    # Attributes can be passed through to the basic recipes
    particles = (;colorfunction = (p->first(p.centerpos)), colorrange = (-1, 2)),
    obstacles = (;color = :gray),
)

display(fig)

plotcb = PeriodicCallback(0.1) do sim
    notify(simplot[1])
    sleep(0.001)
end

evolve!(sim, tmax; callback = plotcb, more_kwargs...)
```

The particle recipe supports coloring particles with custom color functions, which can either map particles to colors
directly or (as illustrated above) use scalar values and predefined color maps with the
standard `cmap`, `colorrange`, `highclip` and `lowclip` attributes

## Interactive tools

InPartSMakie contains a utility function for interactive exploration of 3D simulations, `interactivescene!`,
as well as a simple function `explore_file` for displaying the contents of an InPartS data file.

## Support for custom types

This packages works out of the box with two-dimensional InPartS models that support the legacy `PolyPlottable` interface.
3D models and models that do not provide polygon representations need to implement the function `InPartSMakie.to_mesh(::Particle; kwargs...)`, which
returns a `GeometryBasics` mesh object representing the particle.

For particles that cannot easily be represented by a mesh, it is probably the easiest option to implement a new recipe that accepts similar parameters to
`InPartSMakie.ParticlePlot`, using dispatch over the particle vector element type.
Similarly, particle containers with non-standard objects (Fields, Large Objects, etc.) currently require a custom simulation level plot recipe
(although this may change in future versions of InPartSMakie)